#include <jni.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>

#include "CameraFrameProcessor.h"

using namespace std;
using namespace cv;

extern "C" {

JNIEXPORT void JNICALL Java_hig_colordeficiency_MainActivity_nativeReduceRGB(JNIEnv*, jobject, jlong inputFrame, jfloat r, jfloat g, jfloat b);

JNIEXPORT void JNICALL Java_hig_colordeficiency_MainActivity_nativeReduceRGB(JNIEnv*, jobject, jlong inputFrame, jfloat r, jfloat g, jfloat b)
{
    reduceRGB((Mat*)inputFrame, r, g, b);
}


}
