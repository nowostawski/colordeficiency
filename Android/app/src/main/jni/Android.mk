LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

include /Volumes/Data/OpenCV/opencv_android/sdk/native/jni/OpenCV.mk

LOCAL_MODULE    := jni_main
LOCAL_SRC_FILES := jni_main.cpp CameraFrameProcessor.cpp
LOCAL_LDLIBS +=  -llog -ldl

include $(BUILD_SHARED_LIBRARY)
