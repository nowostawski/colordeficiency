package hig.colordeficiency;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.core.Mat;


/**
 * Main entry point activity.
 */
public class MainActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private CameraBridgeViewBase mOpenCvCameraView;

    private CheckBox redCheckbox;
    private CheckBox redBlindCheckbox;
    private CheckBox greenCheckbox;
    private CheckBox greenBlindCheckbox;
    private CheckBox blueCheckbox;
    private CheckBox blueBlindCheckbox;

    // default color reduction
    private float reduction = 0.6f;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.activity_java_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
        setupUI();
        // Load OpenCV library for Java
        System.loadLibrary("opencv_java");
        System.loadLibrary("jni_main");
    }


    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        mOpenCvCameraView.enableView();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setupUI() {
        this.redCheckbox = (CheckBox) findViewById(R.id.checkbox_red);
        this.redBlindCheckbox = (CheckBox) findViewById(R.id.redFull);

        this.greenCheckbox = (CheckBox) findViewById(R.id.checkbox_green);
        this.greenBlindCheckbox = (CheckBox) findViewById(R.id.greenFull);

        this.blueCheckbox = (CheckBox) findViewById(R.id.checkbox_blue);
        this.blueBlindCheckbox = (CheckBox) findViewById(R.id.blueFull);

        wireUpCheckboxes(this.redCheckbox, this.redBlindCheckbox);
        wireUpCheckboxes(this.greenCheckbox, this.greenBlindCheckbox);
        wireUpCheckboxes(this.blueCheckbox, this.blueBlindCheckbox);
    }


    private void wireUpCheckboxes(final CheckBox color, final CheckBox colorBlind) {
        color.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    colorBlind.setVisibility(View.VISIBLE);
                } else {
                    colorBlind.setChecked(false);
                    colorBlind.setVisibility(View.INVISIBLE);
                }
            }
        });
    }


    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        final Mat m = inputFrame.rgba();
        float   r = 0,
                g = 0,
                b = 0;
        if (redBlindCheckbox.isChecked()) r = 1.0f;
        else if (redCheckbox.isChecked()) r = reduction;

        if (greenBlindCheckbox.isChecked()) g = 1.0f;
        else if (greenCheckbox.isChecked()) g = reduction;

        if (blueBlindCheckbox.isChecked()) b = 1.0f;
        else if (blueCheckbox.isChecked()) b = reduction;


        nativeReduceRGB(m.getNativeObjAddr(), b, g, r);
        // JAVA only
        /*
        final byte[] rgb = new byte[4];
        final int rows = m.rows();
        final int cols = m.cols();
        Log.d("********", "Columns: " + cols + " Rows: " + rows);

        for (int c = 0; c < cols; c++) {
            for (int r = 0; r < rows; r++) {
                m.get(r, c, rgb);
                rgb[0] = 0;
                m.put(r, c, rgb);
            }
        }
        */
        return m;
    }


    native public void nativeReduceRGB(long input, float r, float g, float b);

}
