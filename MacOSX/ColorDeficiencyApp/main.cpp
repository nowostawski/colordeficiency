//
//  main.cpp
//  ColorDeficiencyApp
//
//  Created by Mariusz Nowostawski on 7/09/14.
//  Copyright (c) 2014 Mariusz Nowostawski. All rights reserved.
//

#include <string>
#include <opencv2/opencv.hpp>

#include "CameraFrameProcessor.h"

using namespace cv;


int main(int argc, const char * argv[])
{

    // SETUP the main window
    const char* winTitle = "ColorDeficiencyApp";

    cvNamedWindow( winTitle, CV_WINDOW_AUTOSIZE );
    
    // SETUP the camera
    VideoCapture camera;
    camera.open(0);
    
    Mat cameraFrame;
    
    // Configure the color reduction
    float
        redFactor = .9f,
        blueFactor = 0.1f,
        greenFactor = 0.1f;
    
    
    while(true) {
        startFPSTimer();
        camera >> cameraFrame;
        // we quit the loop if the camera stop feeding us frames
        if (cameraFrame.empty())
            break;
        
        // process and show the video frame
        reduceRGB(&cameraFrame, redFactor, greenFactor, blueFactor);
        showFramesPerSecond(&cameraFrame);
        imshow(winTitle, cameraFrame);
        
        
        // wait 33us for user to be able to press ESC
        char c = cvWaitKey(33);
        if (c == 27)
            break;
        
        stopFPSTimer();
    }
    
    cvDestroyWindow(winTitle);
    return 0;
}

