//
//  CameraFrameProcessor.h
//  ColorDeficiencyApp
//
//  Created by Mariusz Nowostawski on 7/09/14.
//  Copyright (c) 2014 Mariusz Nowostawski. All rights reserved.
//

#ifndef __ColorDeficiencyApp__CameraFrameProcessor__
#define __ColorDeficiencyApp__CameraFrameProcessor__

#include <opencv2/opencv.hpp>
#include <ctime>


using namespace cv;


void reduceRGB(Mat* img, float r, float g, float b);


void showFramesPerSecond(Mat*);

void startFPSTimer();
void stopFPSTimer();




#endif /* defined(__ColorDeficiencyApp__CameraFrameProcessor__) */
