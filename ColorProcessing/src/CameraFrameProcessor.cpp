//
//  CameraFrameProcessor.cpp
//  ColorDeficiencyApp
//
//  Created by Mariusz Nowostawski on 7/09/14.
//  Copyright (c) 2014 Mariusz Nowostawski. All rights reserved.
//

#include "CameraFrameProcessor.h"


void reduceRGB(Mat* img, float r, float g, float b)
{
    int nl = img->rows;
    int nc = img->cols;
    for (int j = 0; j < nl; j++) {
        // get the address of row j
        uchar* data= img->ptr<uchar>(j);
        for (int i = 0; i < nc; i++) {
            //data[0] = 0;//(data[0]/r);// * r + r/2;
            //data[1] = 0;
            int index = (i * 4);
            data[index] = data[index] - data[index] * b;
            data[index+1] = data[index+1] - data[index+1] * g;
            data[index+2] = data[index+2] - data[index+2] * r;
        }
    }
}


// keeps the value of when mainFrameProcessing started
clock_t startProcessingClock;
float rate = 0;

// adds current frames/per/second to the outputFrame
int clockRate = 0;
int clockRateMod = 30;

void startFPSTimer()
{
    startProcessingClock = clock();
        
}

void stopFPSTimer()
{
    clockRate++;
    if (clockRate % clockRateMod == 0) {
        clock_t end = clock();
        rate = end - startProcessingClock;
        rate = CLOCKS_PER_SEC / rate;
        clockRate = 0;
    }
}

char msg[80];
int fontFace = FONT_HERSHEY_COMPLEX;
float fontScale = .8f;
int fontThickness = 2;
Scalar color = CV_RGB(255,0,0);

//
// Adds frames/per/seconds to the output frame
//
void showFramesPerSecond(Mat* img)
{
    sprintf(msg, "Camera FPS: %.1f", rate);
    putText(*img, msg, Point(20,30), fontFace, fontScale, color, fontThickness, CV_AA);
};
